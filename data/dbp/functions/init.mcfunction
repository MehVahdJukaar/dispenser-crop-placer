scoreboard objectives add plant_nw dummy

scoreboard objectives add plant_mine_nw minecraft.mined:minecraft.nether_wart

scoreboard objectives add plant_mine_o_sa minecraft.mined:minecraft.oak_sapling
scoreboard objectives add plant_mine_s_sa minecraft.mined:minecraft.spruce_sapling
scoreboard objectives add plant_mine_b_sa minecraft.mined:minecraft.birch_sapling
scoreboard objectives add plant_mine_j_sa minecraft.mined:minecraft.jungle_sapling
scoreboard objectives add plant_mine_a_sa minecraft.mined:minecraft.acacia_sapling
scoreboard objectives add plant_mine_d_sa minecraft.mined:minecraft.dark_oak_sapling

scoreboard objectives add plant_mine_w_se minecraft.mined:minecraft.wheat
scoreboard objectives add plant_mine_b_se minecraft.mined:minecraft.beetroots
scoreboard objectives add plant_mine_m_se minecraft.mined:minecraft.melon_stem
scoreboard objectives add plant_mine_p_se minecraft.mined:minecraft.pumpkin_stem
scoreboard objectives add plant_mine_c_se minecraft.mined:minecraft.carrots
scoreboard objectives add plant_mine_o_se minecraft.mined:minecraft.potatoes

scoreboard objectives add plant_mine_b_bu minecraft.mined:minecraft.sweet_berry_bush